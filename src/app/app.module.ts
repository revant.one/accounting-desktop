import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { TokenService } from './auth/token/token.service';
import { StorageService } from './auth/storage/storage.service';
import { HttpClientModule } from '@angular/common/http';
import {
  dataStoreFactory,
  DATA_STORE_TOKEN,
} from './auth/data-store/data-store.factory';
import { AccountService } from './ledger/entities/account/account.service';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    TokenService,
    StorageService,
    { provide: DATA_STORE_TOKEN, useFactory: dataStoreFactory },
    AccountService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
