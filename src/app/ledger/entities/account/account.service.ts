import { Inject, Injectable } from '@angular/core';
import { DATA_STORE_TOKEN } from '../../../auth/data-store/data-store.factory';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  model;
  constructor(
    @Inject(DATA_STORE_TOKEN)
    private readonly db: any,
  ) {
    this.model = new this.db('account', {}, {});
  }

  async save(docs) {
    return new Promise((resolve, reject) => {
      this.model.save(docs, (err, docs) => {
        if (err) {
          return reject(err);
        }
        return resolve(docs);
      });
    });
  }

  async findOne(query) {
    return new Promise((resolve, reject) => {
      this.model.findOne(query, (err, doc) => {
        if (err) {
          return reject(err);
        }
        return resolve(doc);
      });
    });
  }
}
