import { Component, OnInit } from '@angular/core';
import { switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { TokenService } from '../auth/token/token.service';
import { LOGGED_IN } from '../auth/token/constants';
import { SET_ITEM, StorageService } from '../auth/storage/storage.service';
// import { AccountService } from '../ledger/entities/account/account.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  name: string;
  email: string;
  picture: string;
  loggedIn: boolean;
  accessToken: string;
  phone: string;

  constructor(
    private readonly token: TokenService,
    private readonly http: HttpClient,
    private readonly store: StorageService, // private readonly ac: AccountService,
  ) {}

  ngOnInit() {
    // this.deleteMe();
    this.store.getItem(LOGGED_IN).then(loggedIn => {
      this.loggedIn = loggedIn === 'true';
    });

    this.store.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === LOGGED_IN) {
          this.loggedIn = res.value?.value === 'true';
          if (this.loggedIn) {
            this.loadProfile();
          }
        }
      },
      error: error => {},
    });

    this.token.config
      .pipe(
        switchMap(config => {
          return this.token.getToken().pipe(
            switchMap(token => {
              this.accessToken = token;
              return this.http.get<any>(config.profileUrl, {
                headers: { authorization: 'Bearer ' + token },
              });
            }),
          );
        }),
      )
      .subscribe({
        next: profile => {
          this.name = profile.name;
          this.email = profile.email;
          this.picture = profile.picture;
        },
        error: error => {},
      });
  }

  loadProfile() {
    this.token.config
      .pipe(
        switchMap(config => {
          return this.token.getToken().pipe(
            switchMap(token => {
              this.accessToken = token;
              return this.http.get<any>(config.profileUrl, {
                headers: { authorization: 'Bearer ' + token },
              });
            }),
          );
        }),
      )
      .subscribe({
        next: profile => {
          this.name = profile.name;
          this.email = profile.email;
          this.picture = profile.picture;
          this.phone = profile.phone_number;
        },
        error: error => {},
      });
  }

  // deleteMe() {
  //   this.ac.save([{ a: "a", b: 'b'}, { a: 1, b: 2 }])
  //     .then(savedDocs => console.log({ savedDocs }));
  //   this.ac.findOne({ a: "a" })
  //     .then(foundDoc => console.log({ foundDoc }));

  // }
}
