import { Component, NgZone } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { Router } from '@angular/router';

import { SET_ITEM, StorageService } from './auth/storage/storage.service';
import { LOGGED_IN } from './auth/token/constants';
import { TokenService } from './auth/token/token.service';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  loggedIn: boolean;
  cap = Plugins.App;

  constructor(
    private readonly token: TokenService,
    private readonly store: StorageService,
    private readonly router: Router,
    private readonly zone: NgZone,
  ) {
    this.initializeApp();
  }

  ngOnInit() {
    this.store.getItem(LOGGED_IN).then(loggedIn => {
      this.loggedIn = loggedIn === 'true';
    });
    this.store.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === LOGGED_IN) {
          this.loggedIn = res.value?.value === 'true';
        }
      },
      error: error => {},
    });
  }

  initializeApp() {
    this.cap.addListener('appUrlOpen', (data: any) => {
      this.zone.run(() => {
        const slug = data.url.split(`${environment.callbackProtocol}://`).pop();
        if (slug) {
          this.router.navigateByUrl(slug);
        }
      });
    });
  }

  logIn() {
    this.token.logIn();
  }

  logOut() {
    this.token.logOut();
  }
}
