import { InjectionToken } from '@angular/core';
import * as LinvoDB from 'linvodb3';
import * as levelJs from 'level-js';

export const DATA_STORE_TOKEN = new InjectionToken('DATA_STORE_TOKEN');

export const dataStoreFactory = () => {
  LinvoDB;
  LinvoDB.defaults.store = { db: levelJs };
  return LinvoDB;
};
